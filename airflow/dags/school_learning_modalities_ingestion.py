import logging
from datetime import datetime, timedelta
from airflow import DAG
from textwrap import dedent
from airflow.providers.postgres.operators.postgres import PostgresOperator
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow.operators.dummy import DummyOperator
from airflow.operators.check_operator import CheckOperator
from airflow.hooks.postgres_hook import PostgresHook

def bulk_upload_to_postgres(copy_sql, filename, conn_id):
    pg_hook = PostgresHook.get_hook(conn_id)
    pg_hook.copy_expert(copy_sql, filename)
    logging.info(f"Bulk upload {filename} into table is done.")

def postgres_xcom_push(sql, conn_id, keys=[], **kwargs):
    pg_hook = PostgresHook.get_hook(conn_id)
    conn = pg_hook.get_conn()
    cur = conn.cursor()
    try:
        logging.info(f"Execute query:\n{sql}")
        cur.execute(sql)
        result = cur.fetchone()
    except Exception as err:
        logging.info(f"Sql run failed: {err}")
        raise
    finally:
        conn.close()
    
    task_instance = kwargs['task_instance']
    output = {}
    for index, key in enumerate(keys):
        output[key] = result[index]
    print(output)
    task_instance.xcom_push(key='return_value', value=output)

FOLDER = "school_learning_modalities"
BASE_PATH = f"/opt/airflow/dags/{FOLDER}"
DATA_PATH = f"/mnt/data/{FOLDER}"

with DAG(
    'school_learning_modalities_ingestion',
    default_args={
        'depends_on_past': False,
        'email': ['leslie.choong94@gmail.com'],
        'email_on_failure': True,
        'email_on_retry': False,
        'retries': 1,
        'retry_delay': timedelta(minutes=5),
        # 'queue': 'bash_queue',
        # 'pool': 'backfill',
        # 'priority_weight': 10,
        # 'end_date': datetime(2016, 1, 1),
        # 'wait_for_downstream': False,
        # 'sla': timedelta(hours=2),
        # 'execution_timeout': timedelta(seconds=300),
        # 'on_failure_callback': some_function,
        # 'on_success_callback': some_other_function,
        # 'on_retry_callback': another_function,
        # 'sla_miss_callback': yet_another_function,
        # 'trigger_rule': 'all_success'
    },
    start_date=datetime(2022, 10, 1),
    schedule_interval="0 0 * * 0",
    catchup=True,
    tags=['glints_assessment']
) as dag:
    
    BATCH_ID = '{{ ds_nodash }}'

    t0 = DummyOperator(
            task_id='pull_data_source_to_landing_dir'
        ) 

    # create 2 files
    # transformed files and corrupted file (unable to preprocess)
    t1 = BashOperator(
        task_id='cleaning_school_modalities',
        bash_command=f"""mkdir -p {DATA_PATH}/{BATCH_ID}/ && \
            mkdir -p {DATA_PATH}/archive/{BATCH_ID}/ && \
            python3 {BASE_PATH}/processing.py \
                    {DATA_PATH}/landing/School_Learning_Modalities.csv \
                    {DATA_PATH}/{BATCH_ID}/school_learning_modality_transform.csv \
                    {DATA_PATH}/{BATCH_ID}/school_learning_modality_corrupted.csv && \ 
            mv {DATA_PATH}/landing/School_Learning_Modalities.csv {DATA_PATH}/archive/{BATCH_ID}/School_Learning_Modalities.csv"""
    )

    t2 = PostgresOperator(
        task_id='manage_ddl_table',
        sql=f'{FOLDER}/sql/ingestion/ddl.sql',
        postgres_conn_id='de-postgres'
    )

    t3 = PythonOperator(
  	    task_id='populate_weekly_ddl_table',
	    python_callable=bulk_upload_to_postgres,
        op_kwargs={
            "copy_sql": "truncate table school_learning_modalities_{{ ds_nodash }}; copy school_learning_modalities_{{ ds_nodash }} from stdin csv delimiter ',';", 
            'filename': f'{DATA_PATH}/{BATCH_ID}/school_learning_modality_transform.csv',
            'conn_id': 'de-postgres'
        }
    )

    t4 = PythonOperator(
        task_id='get_processing_date',
        python_callable=postgres_xcom_push,
        provide_context=True,
        op_kwargs={
            'sql': 'select cast(date(max(week)) as varchar) from school_learning_modalities_{{ ds_nodash }}',
            'keys': ['processing_date'],
            'conn_id': 'de-postgres'
        }
    )

    check = CheckOperator(
        task_id='processing_date_check',
        sql="select count(*) = 0 from school_learning_modalities where cast(week as varchar) = '{{ task_instance.xcom_pull(task_ids='get_processing_date', key='return_value')['processing_date'] }}'",
        conn_id='de-postgres'
    )

    t5 = PostgresOperator(
        task_id='populate_school_learning_modalities_source_table',
        postgres_conn_id='de-postgres',
        sql=f'{FOLDER}/sql/ingestion/school_learning_modalities.sql'
    )

    t6 = PostgresOperator(
        task_id='populate_latest_version_table',
        postgres_conn_id='de-postgres',
        sql=f'{FOLDER}/sql/ingestion/school_learning_modalities_latest.sql'
    )
   
    t7 = PostgresOperator(
        task_id='summary_stats_top_5_district',
        postgres_conn_id='de-postgres',
        sql=f'{FOLDER}/sql/summary_stat/top_5_district.sql'
    )

    t8 = PostgresOperator(
        task_id='summary_stats_state_hybrid_agg',
        postgres_conn_id='de-postgres',
        sql=f'{FOLDER}/sql/summary_stat/state_hybrid_agg.sql'
    )

    t9 = PostgresOperator(
        task_id='summary_stats_first_and_last_week_stat',
        postgres_conn_id='de-postgres',
        sql=f'{FOLDER}/sql/summary_stat/first_and_last_week_stat.sql'
    )

    t_success = DummyOperator(
        task_id='ingestion_success'
    )
    t0 >> t1 >> t2 >> t3 >> t4 >> check >> t5 >> t6 >> [t7,t8,t9] >> t_success
