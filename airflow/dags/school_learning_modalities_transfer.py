import logging
from datetime import datetime, timedelta
from airflow import DAG
from airflow.providers.postgres.operators.postgres import PostgresOperator
from airflow.operators.python import PythonOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.sensors.external_task import ExternalTaskSensor

import uuid
import os

def truncate_table(hook, table_name):
    conn = hook.get_conn()
    cur = conn.cursor()
    try:
        cur.execute(f"truncate table {table_name};")
        conn.commit()
    except Exception as err:
        print(f"truncate err: {err}")
    finally:
        conn.close()
    print(f"{table_name} is truncated")


def bulk_transfer_postgres_to_postregs(from_conn, to_conn, target_table_name):
    # generate file
    tmp_file = f'/tmp/{uuid.uuid4().hex}'
    with open(tmp_file, "w+") as f:
        pass

    # get connection
    from_pg_hook = PostgresHook(from_conn)
    to_pg_hook = PostgresHook(to_conn)
    from_pg_hook = from_pg_hook.bulk_dump(target_table_name, tmp_file)
    logging.info(f'bulk download from {target_table_name} to {tmp_file}')
    
    # load to tables
    truncate_table(to_pg_hook, target_table_name)
    to_pg_hook = to_pg_hook.bulk_load(target_table_name, tmp_file)
    logging.info(f'bulk upload from {target_table_name} from {tmp_file}')
    os.remove(tmp_file)

FOLDER = "school_learning_modalities"
BASE_PATH = f"/opt/airflow/dags/{FOLDER}"
DATA_PATH = f"/mnt/data/{FOLDER}"

with DAG(
    'school_learning_modalities_transfer',
    default_args={
        'depends_on_past': False,
        'email': ['leslie.choong94@gmail.com'],
        'email_on_failure': True,
        'email_on_retry': False,
        'retries': 1,
        'retry_delay': timedelta(minutes=5),
        # 'queue': 'bash_queue',
        # 'pool': 'backfill',
        # 'priority_weight': 10,
        # 'end_date': datetime(2016, 1, 1),
        # 'wait_for_downstream': False,
        # 'sla': timedelta(hours=2),
        # 'execution_timeout': timedelta(seconds=300),
        # 'on_failure_callback': some_function,
        # 'on_success_callback': some_other_function,
        # 'on_retry_callback': another_function,
        # 'sla_miss_callback': yet_another_function,
        # 'trigger_rule': 'all_success'
    },
    start_date=datetime(2022, 10, 1),
    schedule_interval="0 0 * * 0",
    catchup=False,
    tags=['glints_assessment']
) as dag:
    t0 = ExternalTaskSensor(
        task_id="waiting_school_learning_modalities_ingestion",
        external_dag_id="school_learning_modalities_ingestion",
        external_task_id="ingestion_success",
        execution_delta=timedelta(minutes=0),
        timeout=10,
        retries=120,
        retry_delay=timedelta(minutes=5)
    )

    t1 = PostgresOperator(
        task_id="manage_summary_stats_ddl",
        postgres_conn_id="da-postgres",
        sql=f"{FOLDER}/sql/tableau/ddl.sql"
    )
    
    t2 = PythonOperator(
        task_id='bulk_transfer_school_learning_modalities_latest',
        python_callable=bulk_transfer_postgres_to_postregs,
        op_kwargs={
            'from_conn': 'de-postgres',
            'to_conn': 'da-postgres',
            'target_table_name': 'school_learning_modalities_latest'
        }
    )

    t3 = PythonOperator(
        task_id='bulk_transfer_summary_stat_first_week_last_week_comparison',
        python_callable=bulk_transfer_postgres_to_postregs,
        op_kwargs={
            'from_conn': 'de-postgres',
            'to_conn': 'da-postgres',
            'target_table_name': 'summary_stat_first_week_last_week_comparison'
        }
    )

    t4 = PythonOperator(
        task_id='bulk_transfer_summary_stat_state_hybrid_metrics_agg',
        python_callable=bulk_transfer_postgres_to_postregs,
        op_kwargs={
            'from_conn': 'de-postgres',
            'to_conn': 'da-postgres',
            'target_table_name': 'summary_stat_state_hybrid_metrics_agg'
        }
    )

    t5 = PythonOperator(
        task_id='bulk_transfer_summary_stat_top5_district',
        python_callable=bulk_transfer_postgres_to_postregs,
        op_kwargs={
            'from_conn': 'de-postgres',
            'to_conn': 'da-postgres',
            'target_table_name': 'summary_stat_top5_district'
        }
    )

    t0 >> t1 >> [t2, t3, t4, t5]
