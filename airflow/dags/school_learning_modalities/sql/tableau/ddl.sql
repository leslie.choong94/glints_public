create table if not exists school_learning_modalities_latest (
  district_nces_id NUMERIC NOT NULL,
  district_name VARCHAR(100) NOT NULL,
  week TIMESTAMP NOT NULL,
  learning_modality VARCHAR(20) NOT NULL,
  operational_schools NUMERIC NOT NULL,
  student_count NUMERIC NOT NULL,
  city VARCHAR(50) NOT NULL,
  state VARCHAR(20) NOT NULL,
  zip_code NUMERIC NOT NULL,
  PRIMARY KEY (district_nces_id)
);

create table if not exists summary_stat_first_week_last_week_comparison
(
        district_nces_id numeric not null,
        first_week_student_count numeric,
        last_week_student_count numeric
);

create table if not exists summary_stat_state_hybrid_metrics_agg
(
        state varchar(5) NOT NULL,
        updated_date date NOT NULL,
        total_hybrid_student_count numeric NOT NULL,
        student_count numeric NOT NULL,
        total_hybrid_operational_school numeric NOT NULL,
        total_operational_school numeric NOT NULL,
        unique(state, updated_date)
);

create table if not exists summary_stat_top5_district
(
        district_nces_id numeric,
        district_name varchar(50),
        student_count numeric,
        rank integer,
        rank_date date
);

