create table if not exists summary_stat_first_week_last_week_comparison
(
	district_nces_id numeric not null,
	first_week_student_count numeric,
	last_week_student_count numeric
);

truncate table summary_stat_first_week_last_week_comparison;

insert into summary_stat_first_week_last_week_comparison
with t1 as 
(
	select district_nces_id, student_count, extract(week from week) as week_number from school_learning_modalities_20221021 
	where extract(month from date(week)) = extract(month from date('{{ task_instance.xcom_pull(task_ids='get_processing_date', key='return_value')['processing_date'] }}'))
		
), 
min_max as
(
	select min(week_number) as first_week_in_month, max(week_number) as latest_week_in_month from t1
), 
first_week as
(
	select
       		district_nces_id, student_count first_week_student_count	
	from t1
	inner join min_max
	on t1.week_number = min_max.first_week_in_month
),
latest_week as 
(
	select 
		district_nces_id, student_count latest_week_student_count
	from t1
	inner join min_max
	on t1.week_number = min_max.latest_week_in_month
)
select 
	coalesce(first_week.district_nces_id, latest_week.district_nces_id) as district_nces_id,
	first_week_student_count,
	latest_week_student_count
from first_week
full join latest_week
on first_week.district_nces_id = latest_week.district_nces_id;
