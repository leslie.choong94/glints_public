create table if not exists summary_stat_state_hybrid_metrics_agg
(
	state varchar(5) NOT NULL,
	updated_date date NOT NULL,
	total_hybrid_student_count numeric NOT NULL,
	student_count numeric NOT NULL,
	total_hybrid_operational_school numeric NOT NULL,
	total_operational_school numeric NOT NULL,
	unique(state, updated_date)
);


with upsert as
(
select
        state,
        cast(max(week) as date) as updated_date,
        sum(case when learning_modality = 'Hybrid' then student_count else 0 end) as total_hybrid_student_count,
        sum(student_count) as student_count,
        sum(case when learning_modality = 'Hybrid' then operational_schools else 0 end) as total_hybrid_operational_schools,
        sum(operational_schools) total_operational_schools
from
       school_learning_modalities_latest
where
        learning_modality = 'Hybrid'
group by 1
)
insert into summary_stat_state_hybrid_metrics_agg
select * from upsert
ON CONFLICT(state, updated_date) DO UPDATE SET (state, updated_date, total_hybrid_student_count, student_count, total_hybrid_operational_school, total_operational_school) =  (EXCLUDED.state, EXCLUDED.updated_date, EXCLUDED.total_hybrid_student_count, EXCLUDED.student_count, EXCLUDED.total_hybrid_operational_school, EXCLUDED.total_operational_school);

