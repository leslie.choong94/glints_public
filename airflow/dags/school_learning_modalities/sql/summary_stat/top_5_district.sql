create table if not exists summary_stat_top5_district
(
	district_nces_id numeric,
	district_name varchar(50),
	student_count numeric,
	rank integer,
	rank_date date
);

delete from summary_stat_top5_district
where rank_date = '{{ task_instance.xcom_pull(task_ids='get_processing_date', key='return_value')['processing_date'] }}'
returning *;

insert into summary_stat_top5_district
select 
	district_nces_id,
        district_name,	
	student_count, 
	row_number() over(order by student_count desc) as rank,
	date(week) as rank_date	
from 
	school_learning_modalities_latest 
where 
	date(week) = '{{ task_instance.xcom_pull(task_ids='get_processing_date', key='return_value')['processing_date'] }}' 
order by rank limit 5;



