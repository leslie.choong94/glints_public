create table if not exists school_learning_modalities_latest (
  district_nces_id NUMERIC NOT NULL,
  district_name VARCHAR(100) NOT NULL,
  week TIMESTAMP NOT NULL,
  learning_modality VARCHAR(20) NOT NULL,
  operational_schools NUMERIC NOT NULL,
  student_count NUMERIC NOT NULL,
  city VARCHAR(50) NOT NULL,
  state VARCHAR(20) NOT NULL,
  zip_code NUMERIC NOT NULL,
  PRIMARY KEY (district_nces_id)
);

create temp table slm_temp_{{ ds_nodash }} as
with dedup as 
 (
     select *,
            rank() over (partition by district_nces_id order by week desc) rn
     from school_learning_modalities_{{ ds_nodash }} 
)
select district_nces_id, district_name, week, learning_modality, operational_schools, student_count, city, state, zip_code
from dedup
where rn = 1;

INSERT INTO school_learning_modalities_latest
SELECT district_nces_id, district_name, week, learning_modality, operational_schools, student_count, city, state, zip_code
FROM slm_temp_{{ ds_nodash }}
ON CONFLICT (district_nces_id) 
DO UPDATE SET (district_name, week, operational_schools, student_count, city, state, zip_code) = (EXCLUDED.district_name, EXCLUDED.week, EXCLUDED.operational_schools, EXCLUDED.student_count, EXCLUDED.city, EXCLUDED.state, EXCLUDED.zip_code) 
WHERE school_learning_modalities_latest.district_nces_id = EXCLUDED.district_nces_id 
AND school_learning_modalities_latest.week < EXCLUDED.week;
