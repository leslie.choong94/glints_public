create table if not exists school_learning_modalities (
  district_nces_id NUMERIC NOT NULL,
  district_name VARCHAR(100) NOT NULL,
  week TIMESTAMP NOT NULL,
  learning_modality VARCHAR(20) NOT NULL,
  operational_schools NUMERIC NOT NULL,
  student_count NUMERIC NOT NULL,
  city VARCHAR(50) NOT NULL,
  state VARCHAR(5) NOT NULL,
  zip_code NUMERIC NOT NULL,
  PRIMARY KEY (district_nces_id, week)
);

drop table if exists school_learning_modalities_{{ ds_nodash }};

create table if not exists school_learning_modalities_{{ ds_nodash }} (
  district_nces_id NUMERIC NOT NULL,
  district_name VARCHAR(100) NOT NULL,
  week TIMESTAMP NOT NULL,
  learning_modality VARCHAR(20) NOT NULL,
  operational_schools NUMERIC NOT NULL,
  student_count NUMERIC NOT NULL,
  city VARCHAR(50) NOT NULL,
  state VARCHAR(5) NOT NULL,
  zip_code NUMERIC NOT NULL,
  PRIMARY KEY (district_nces_id, week)
);

drop table if exists school_learning_modalities_{{ (execution_date - macros.timedelta(days=56)).strftime("%Y%m%d") }};

