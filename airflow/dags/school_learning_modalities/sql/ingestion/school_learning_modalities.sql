drop table if exists school_learning_modalities;

create table if not exists school_learning_modalities (
  district_nces_id NUMERIC NOT NULL,
  district_name VARCHAR(100) NOT NULL,
  week TIMESTAMP NOT NULL,
  learning_modality VARCHAR(20) NOT NULL,
  operational_schools NUMERIC NOT NULL,
  student_count NUMERIC NOT NULL,
  city VARCHAR(50) NOT NULL,
  state VARCHAR(20) NOT NULL,
  zip_code NUMERIC NOT NULL,
  ingestion_batch VARCHAR(8) NOT NULL,
  PRIMARY KEY (ingestion_batch, district_nces_id, week)
);

delete from school_learning_modalities where ingestion_batch = '{{ ds_nodash }}' returning *;

insert into school_learning_modalities (district_nces_id, district_name, week, learning_modality, operational_schools, student_count, city, state, zip_code, ingestion_batch) 
select district_nces_id, district_name, week, learning_modality, operational_schools, student_count, city, state, zip_code, '{{ ds_nodash}}' as ingestion_batch from school_learning_modalities_{{ ds_nodash }};
