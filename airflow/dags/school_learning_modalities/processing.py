import csv
import argparse
from datetime import datetime

##header = [
##    'district_nces_id',
##    'district_name',
##    'week',
##    'learning_modality',
##    'operational_schools',
##    'student_count',
##    'city',
##    'state',
##    'zip_code'
##]

def transformation(row):
    row[0] = district_nces_id_clean(row[0])
    row[2] = week_clean(row[2])
    row[4] = int(row[4])
    row[5] = student_count_clean(row[5])
    row[8] = int(row[8])
    return row

def district_nces_id_clean(val):
    return int(val.lstrip('0'))

def week_clean(date_string):
    format = "%m/%d/%Y %I:%M:%S %p"
    to_format = "%Y-%m-%d %H:%M:%S"
    return datetime.strptime(date_string, format).strftime(to_format)

def student_count_clean(num_string):
    if len(num_string.replace(',', '')) <= 1:
        return 0
    return int(num_string.replace(',', ''))

def main(args):
    # init 
    input_csv_path = args.input_path
    output_csv_path = args.output_path
    corrupted_csv_path = args.corrupted_path
    corrupted_rows = []
    line_count = 0
    print("Transformation started..")
    with open(input_csv_path, 'r') as f:
        print(f"Reading records from {input_csv_path}")
        reader = csv.reader(f)
        header = next(reader, None)
        with open(output_csv_path, 'w', newline='') as output_file:
            csv_writer = csv.writer(output_file)
            for row in reader:
                try:
                    new_row = transformation(row)
                    csv_writer.writerow(new_row)
                    line_count += 1
                except Exception as err:
                    print(f'Row parsing error: {err}')
                    print(row)
                    corrupted_rows.append(row)
    print(f"{line_count} transformed rows were populated into {output_csv_path}.")

    print(f"Writing {len(corrupted_rows)} corrupted rows into {corrupted_csv_path}.")
    with open(corrupted_csv_path, 'w', newline='') as corrupted_csv:
        csv_writer = csv.writer(corrupted_csv)
        for row in corrupted_rows:
            csv_writer.writerow(row)
    print("Processing done")
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='process csv')
    parser.add_argument('input_path')
    parser.add_argument('output_path')
    parser.add_argument('corrupted_path')
    args = parser.parse_args()
    main(args)

