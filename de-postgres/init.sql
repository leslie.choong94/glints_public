CREATE DATABASE school_learning_model;
CREATE USER glints_de WITH PASSWORD 'glints-de-pass';
GRANT ALL PRIVILEGES ON DATABASE "school_learning_model" to glints_de;
GRANT pg_read_server_files TO glints_de;
