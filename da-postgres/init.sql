CREATE DATABASE school_learning_model_tableau;
CREATE USER glints_de WITH PASSWORD 'glints-de-pass';
GRANT ALL PRIVILEGES ON DATABASE "school_learning_model_tableau" to glints_de;
GRANT pg_read_server_files TO glints_de;
