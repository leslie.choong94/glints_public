## Instruction
1. ssh into an ec2 instance (Amazon Linux, t3.xlarge) 
2. run line by line from setup.sh in ec2 instance
3. upload School_Learning_Modalities.csv to ~/glints_public/airflow/mnt/data/school_learning_modalities/landing/School_Learning_Modalities.csv
4. navigate to http://<ec2_public_ip_address>:5884
5. add the connections to the airflow connection page
```
    - connection_type: postgresql
    - conn_name: de-postgres
    - host: de-postgres
    - username: glints_de
    - port: 5432
    - db_schema: school_learning_model
    - password: glints-de-pass
```
```
    - connection_type: postgresql
    - conn_name: da-postgres
    - host: da-postgres
    - username: glints_de
    - port: 5432
    - db_schema: school_learning_model_tableau
    - password: glints-de-pass
```
( Sorry for the inconvinience, Docker is not available on my current laptop)

## Other Credentials
airflow
- username: airflow
- password: airflow

## code improvement plan
- airflow with celery executor (redis, airflow-worker, celery related airflow config change)
- write custom operators to dump data between 2 postgres db
- hide password with docker secret/integrate with vault

## ~WIP - By 22/10/2022 10:00pm~
- ~summary stats in Database X~
- ~pipelines for Database Y~

## Dataset in target database Y

- Compare “Learning Modality” across various Districts
    - maintain a "latest" version to compare across district
    - design to catch miss weekly update for a particular district

```sql
create table if not exists school_learning_modalities_latest (
  district_nces_id NUMERIC NOT NULL,
  district_name VARCHAR(100) NOT NULL,
  week TIMESTAMP NOT NULL,
  learning_modality VARCHAR(20) NOT NULL,
  operational_schools NUMERIC NOT NULL,
  student_count NUMERIC NOT NULL,
  city VARCHAR(50) NOT NULL,
  state VARCHAR(20) NOT NULL,
  zip_code NUMERIC NOT NULL,
  PRIMARY KEY (district_nces_id)
);
```

- Any growth of Students, in District wise when we compare the first week and last week of October 2022.
    - didnt design to catch any missing updates

```sql
create table if not exists summary_stat_first_week_last_week_comparison
(
        district_nces_id numeric not null,
        first_week_student_count numeric,
        last_week_student_count numeric
);
```

- States Focusing more in “Hybrid” way of “Learning Modality”
    - compare the number or percentage to justify which states is "more focusing"

```sql
create table if not exists summary_stat_state_hybrid_metrics_agg
(
        state varchar(5) NOT NULL,
        updated_date date NOT NULL,
        total_hybrid_student_count numeric NOT NULL,
        student_count numeric NOT NULL,
        total_hybrid_operational_school numeric NOT NULL,
        total_operational_school numeric NOT NULL,
        unique(state, updated_date)
);
```

- top 5 district
```sql
create table if not exists summary_stat_top5_district
(
        district_nces_id numeric,
        district_name varchar(50),
        student_count numeric,
        rank integer,
        rank_date date
);
```

