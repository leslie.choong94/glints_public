#!/bin/bash

sudo yum -y update
sudo yum install -y git
git clone https://gitlab.com/leslie.choong94/glints_public.git

sudo yum -y install docker
sudo usermod -a -G docker ec2-user
id ec2-user
newgrp docker

wget https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) 
sudo mv docker-compose-$(uname -s)-$(uname -m) /usr/local/bin/docker-compose
sudo chmod -v +x /usr/local/bin/docker-compose

# sudo systemctl enable docker.service
sudo service docker start

mkdir -p ~/glints_public/airflow-postgres/data
mkdir -p ~/glints_public/de-postgres/data
mkdir -p ~/glints_public/da-postgres/data
mkdir -p ~/glints_public/airflow/dags
mkdir -p ~/glints_public/airflow/logs
mkdir -p ~/glints_public/airflow/plugins

cd ~/glints_public/

docker-compose up -d
